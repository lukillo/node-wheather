FROM node:10.16.3-alpine
LABEL Maintainer="Lucas Llopis"
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run test
EXPOSE 3000
CMD [ "node", "./src/server/app.js" ]