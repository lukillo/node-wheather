//Include unit testing libs.
const chai       = require('chai');
const proxyquire = require('proxyquire');
const sinon      = require('sinon');

//Add chai as promise.
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const mockGet = sinon.stub()
const mockConfig ={
    get : mockGet
};
mockGet.withArgs('wheater').returns('https://api.openweathermap.org/data/2.5');
mockGet.withArgs('apiKey').returns('MY_API_KEY');

const mockFetchResponse = {
    status: 200,
    json: ()=>(true)
}
const mockFetchResponse2 = {
    status: 500
}
const mockFetch = sinon.stub();
mockFetch.withArgs('https://api.openweathermap.org/data/2.5/weather?q=Buenos%20Aires&units=metric&appid=MY_API_KEY', { method: 'GET'})
.resolves(mockFetchResponse);
mockFetch.withArgs('https://api.openweathermap.org/data/2.5/weather?q=Cordoba&units=metric&appid=MY_API_KEY', { method: 'GET'})
.resolves(mockFetchResponse2);


const {
    fetchWheather
  } = proxyquire('../../src/accessors/wheather',{
    'node-fetch': mockFetch,
    'config': mockConfig,
  })

describe('Test Wheater Accessors', ()=>{
    beforeEach(()=>{
        mockFetch.resetHistory()
    })
    it('fetchWheather test', async()=>{
        const result = await fetchWheather('Buenos%20Aires');
        chai.expect(mockFetch.calledOnce).to.be.true;
        chai.expect(result).to.be.true;
    })
    it('fetchWheather rejected', async()=>{
        await chai.expect(fetchWheather('Cordoba')).to.be.rejected;
    })
});