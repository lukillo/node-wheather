//Include unit testing libs.
const chai       = require('chai');
const proxyquire = require('proxyquire');
const sinon      = require('sinon');

//Add chai as promise.
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const mockGet = sinon.stub()
const mockConfig ={
    get : mockGet
};
mockGet.withArgs('ipApi').returns('http://ip-api.com/json');

const mockFetchResponse = {
    status: 200,
    json: ()=>(true)
}
const mockFetchResponse2 = {
    status: 500
}
const mockFetch = sinon.stub();
mockFetch.withArgs('http://ip-api.com/json/192.168.0.1', { method: 'GET'})
.resolves(mockFetchResponse);
mockFetch.withArgs('http://ip-api.com/json/192.168.0.2', { method: 'GET'})
.resolves(mockFetchResponse2);


const {
    fetchLocation
  } = proxyquire('../../src/accessors/location',{
    'node-fetch': mockFetch,
    'config': mockConfig,
  })

describe('Test fetchLocation Accessors', ()=>{
    beforeEach(()=>{
        mockFetch.resetHistory()
    })
    it('fetchLocation test', async()=>{
        const result = await fetchLocation('192.168.0.1');
        chai.expect(mockFetch.calledOnce).to.be.true;
        chai.expect(result).to.be.true;
    })
    it('fetchLocation rejected', async()=>{
        await chai.expect(fetchLocation('192.168.0.2')).to.be.rejected;
    })
});