//Include unit testing libs.
const chai       = require('chai');
const proxyquire = require('proxyquire');
const sinon      = require('sinon');

//Add chai as promise.
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const mockGet = sinon.stub()
const mockConfig ={
    get : mockGet
};
mockGet.withArgs('wheater').returns('https://api.openweathermap.org/data/2.5');
mockGet.withArgs('apiKey').returns('MY_API_KEY');

const mockFetchResponse = {
    status: 200,
    json: ()=>(true)
}
const mockFetchResponse2 = {
    status: 500
}
const mockFetch = sinon.stub();
mockFetch.withArgs('https://api.openweathermap.org/data/2.5/forecast?q=Buenos%20Aires&units=metric&cnt=5&appid=MY_API_KEY', { method: 'GET'})
.resolves(mockFetchResponse);
mockFetch.withArgs('https://api.openweathermap.org/data/2.5/forecast?q=Cordoba&units=metric&cnt=5&appid=MY_API_KEY', { method: 'GET'})
.resolves(mockFetchResponse2);


const {
    fetchForecast
  } = proxyquire('../../src/accessors/forecast',{
    'node-fetch': mockFetch,
    'config': mockConfig,
  })

describe('Test Forecast Accessors', ()=>{
    beforeEach(()=>{
        mockFetch.resetHistory()
    })
    it('fetchForecast test', async()=>{
        const result = await fetchForecast('Buenos%20Aires');
        chai.expect(mockFetch.calledOnce).to.be.true;
        chai.expect(result).to.be.true;
    })
    it('fetchForecast rejected', async()=>{
        await chai.expect(fetchForecast('Cordoba')).to.be.rejected;
    })
});