const chai  = require('chai');
const getIp = require('../../src/lib/ip');

describe('Test ip lib', ()=>{
    it('ip - With x-forwarded-for',  ()=>{
    
        const req = {
            headers:{
                'x-forwarded-for': '190.192.81.14'
            }
        };
         
        chai.expect(getIp(req)).to.be.eql('190.192.81.14');
      });
    
    it('ip - With remoteAddress',  ()=>{

        const req = {
            headers:{
                'x-forwarded-for': null
            },
            connection:{
                remoteAddress: '190.192.81.14'
            }
        };
            
        chai.expect(getIp(req)).to.be.eql('190.192.81.14');
    }); 
    
    it('ip - With socket',  ()=>{

        const req = {
            headers:{
                'x-forwarded-for': null
            },
            connection:{
                remoteAddress: null
            },
            socket:{
                remoteAddress: '190.192.81.14'
            }
        };
            
        chai.expect(getIp(req)).to.be.eql('190.192.81.14');
    });

    it('ip - With connection socket remoteAddress',  ()=>{

        const req = {
            headers:{
                'x-forwarded-for': null
            },
            connection:{
                socket: {
                    remoteAddress: '190.192.81.14'
                }
            },
            socket:{
                remoteAddress: null
            }
        };
            
        chai.expect(getIp(req)).to.be.eql('190.192.81.14');
    });

    it('ip - all null',  ()=>{

        const req = {
            headers:{
                'x-forwarded-for': null
            },
            connection:{
                socket: {
                    remoteAddress: null
                }
            },
            socket:{
                remoteAddress: null
            }
        };
            
        chai.expect(getIp(req)).to.be.eql(null);
    });

    it('ip - socket remoteAddress',  ()=>{

        const req = {
            headers:{
                'x-forwarded-for': null
            },
            connection:{
                socket: {
                    remoteAddress: '190.192.81.14'
                }
            },
            socket:{
                remoteAddress: null
            }
        };
            
        chai.expect(getIp(req)).to.be.eql('190.192.81.14');
    });

    it('ip - socket remoteAddress null',  ()=>{

        const req = {
            headers:{
                'x-forwarded-for': null
            },
            connection:{
                socket: null
            },
            socket:{
                remoteAddress: null
            }
        };
            
        chai.expect(getIp(req)).to.be.eql(null);
    });

    it('ip - regex test',  ()=>{

        const req = {
            headers:{
                'x-forwarded-for': '::ff:190.192.81.14'
            }
        };
            
        chai.expect(getIp(req)).to.be.eql('190.192.81.14');
    });
})