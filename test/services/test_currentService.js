const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const mockResponseWheater = {
    "coord": {
        "lon": -58.38,
        "lat": -34.61
    },
    "weather": [
        {
            "id": 800,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 11.77,
        "feels_like": 8.01,
        "temp_min": 11,
        "temp_max": 12.22,
        "pressure": 1015,
        "humidity": 53
    },
    "visibility": 10000,
    "wind": {
        "speed": 3.1,
        "deg": 360
    },
    "clouds": {
        "all": 0
    },
    "dt": 1594481097,
    "sys": {
        "type": 1,
        "id": 8224,
        "country": "AR",
        "sunrise": 1594465151,
        "sunset": 1594501114
    },
    "timezone": -10800,
    "id": 3435910,
    "name": "Buenos Aires",
    "cod": 200
}
const mockLocationResponse = {"status":"success","country":"Argentina","countryCode":"AR","region":"C","regionName":"Buenos Aires F.D.","city":"Buenos Aires","zip":"1871","lat":-34.6021,"lon":-58.3845,"timezone":"America/Argentina/Buenos_Aires","isp":"Telecom Argentina S.A.","org":"Telecom Argentina S.A","as":"AS10481 Telecom Argentina S.A.","query":"190.192.81.14"}
const mockLocationService = sinon.stub().resolves(mockLocationResponse);
const mockFetchWheather = sinon.stub().resolves(mockResponseWheater);
const mockWheater = {
    fetchWheather: mockFetchWheather,
}

const { getWheaterByCity, getWheaterByIp } = proxyquire('../../src/services/currentService', {
    './locationService': mockLocationService,
    '../accessors/wheather': mockWheater
})

describe('Test Current Service', ()=>{
    beforeEach(()=>{
        mockFetchWheather.resetHistory();
        mockLocationService.resetHistory();
    })
    it('getWheaterByCity - Call ok with city name', async ()=>{
    
        const result = await getWheaterByCity('Buenos%20Aires');
        chai.expect(result).to.be.eql(mockResponseWheater);
        chai.expect(mockFetchWheather.calledOnce).to.be.true;
        
    });
    it('getWheaterByIp - Call ok with ip', async ()=>{
    
        const result = await getWheaterByIp('192.168.0.1');
        chai.expect(result).to.be.eql(mockResponseWheater);
        chai.expect(mockFetchWheather.calledOnce).to.be.true;
        chai.expect(mockLocationService.calledOnce).to.be.true;
        
    });
})