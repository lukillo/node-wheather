const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const mockResponseForecast = {
    "cod": "200",
    "message": 0,
    "cnt": 5,
    "list": [
        {
            "dt": 1594490400,
            "main": {
                "temp": 12.27,
                "feels_like": 8.13,
                "temp_min": 12.21,
                "temp_max": 12.27,
                "pressure": 1014,
                "sea_level": 1012,
                "grnd_level": 1008,
                "humidity": 46,
                "temp_kf": 0.06
            },
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": {
                "all": 0
            },
            "wind": {
                "speed": 3.29,
                "deg": 336
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2020-07-11 18:00:00"
        },
        {
            "dt": 1594501200,
            "main": {
                "temp": 12.08,
                "feels_like": 7.1,
                "temp_min": 12.01,
                "temp_max": 12.08,
                "pressure": 1012,
                "sea_level": 1011,
                "grnd_level": 1007,
                "humidity": 45,
                "temp_kf": 0.07
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "clouds": {
                "all": 45
            },
            "wind": {
                "speed": 4.38,
                "deg": 330
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2020-07-11 21:00:00"
        },
        {
            "dt": 1594512000,
            "main": {
                "temp": 11.18,
                "feels_like": 6.93,
                "temp_min": 11.1,
                "temp_max": 11.18,
                "pressure": 1013,
                "sea_level": 1013,
                "grnd_level": 1008,
                "humidity": 46,
                "temp_kf": 0.08
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 74
            },
            "wind": {
                "speed": 3.24,
                "deg": 301
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2020-07-12 00:00:00"
        },
        {
            "dt": 1594522800,
            "main": {
                "temp": 10.53,
                "feels_like": 6.48,
                "temp_min": 10.52,
                "temp_max": 10.53,
                "pressure": 1013,
                "sea_level": 1013,
                "grnd_level": 1008,
                "humidity": 47,
                "temp_kf": 0.01
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 79
            },
            "wind": {
                "speed": 2.89,
                "deg": 277
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2020-07-12 03:00:00"
        },
        {
            "dt": 1594533600,
            "main": {
                "temp": 10.32,
                "feels_like": 6.16,
                "temp_min": 10.32,
                "temp_max": 10.32,
                "pressure": 1013,
                "sea_level": 1013,
                "grnd_level": 1008,
                "humidity": 46,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 90
            },
            "wind": {
                "speed": 2.94,
                "deg": 286
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2020-07-12 06:00:00"
        }
    ],
    "city": {
        "id": 3435910,
        "name": "Buenos Aires",
        "coord": {
            "lat": -34.6132,
            "lon": -58.3772
        },
        "country": "AR",
        "population": 1000000,
        "timezone": -10800,
        "sunrise": 1594465151,
        "sunset": 1594501113
    }
}
const mockLocationResponse = {"status":"success","country":"Argentina","countryCode":"AR","region":"C","regionName":"Buenos Aires F.D.","city":"Buenos Aires","zip":"1871","lat":-34.6021,"lon":-58.3845,"timezone":"America/Argentina/Buenos_Aires","isp":"Telecom Argentina S.A.","org":"Telecom Argentina S.A","as":"AS10481 Telecom Argentina S.A.","query":"190.192.81.14"}
const mockLocationService = sinon.stub().resolves(mockLocationResponse);
const mockFetchForecast = sinon.stub().resolves(mockResponseForecast);
const mockForecast = {
    fetchForecast: mockFetchForecast,
}

const { getForecastByCity, getForecastByIp } = proxyquire('../../src/services/forecastService', {
    './locationService': mockLocationService,
    '../accessors/forecast': mockForecast
})

describe('Test Forecast Service', ()=>{
    beforeEach(()=>{
        mockFetchForecast.resetHistory();
        mockLocationService.resetHistory();
    })
    it('getForecastByCity - Call ok with city name', async ()=>{
    
        const result = await getForecastByCity('Buenos%20Aires');
        chai.expect(result).to.be.eql(mockResponseForecast);
        chai.expect(mockFetchForecast.calledOnce).to.be.true;
        
    });
    it('getForecastByIp - Call ok with ip', async ()=>{
    
        const result = await getForecastByIp('192.168.0.1');
        chai.expect(result).to.be.eql(mockResponseForecast);
        chai.expect(mockFetchForecast.calledOnce).to.be.true;
        chai.expect(mockLocationService.calledOnce).to.be.true;
        
    });
})