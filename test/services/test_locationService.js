const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const mockLocationResponse = {"status":"success","country":"Argentina","countryCode":"AR","region":"C","regionName":"Buenos Aires F.D.","city":"Buenos Aires","zip":"1871","lat":-34.6021,"lon":-58.3845,"timezone":"America/Argentina/Buenos_Aires","isp":"Telecom Argentina S.A.","org":"Telecom Argentina S.A","as":"AS10481 Telecom Argentina S.A.","query":"190.192.81.14"}
const mockLocationService = sinon.stub().resolves(mockLocationResponse);

const mockLocation = {
    fetchLocation: mockLocationService,
}

const getLocation = proxyquire('../../src/services/locationService', {
    '../accessors/location': mockLocation
})

describe('Test Locarion Service', ()=>{
    it('getLocation - Call ok with ip', async ()=>{
    
        const result = await getLocation('192.168.0.1');
        chai.expect(result).to.be.eql(mockLocationResponse);
        chai.expect(mockLocationService.calledOnce).to.be.true;
        
    });
})