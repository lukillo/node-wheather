const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const mockConsole = {
    info: sinon.fake(),
    error: sinon.fake()
}
const mockProcess = {
    exit: sinon.fake()
};
const mockServer = {
    close: callback => {
        callback()
    }
};
const { onListen, onServerError , onException } = require('../../src/server/events')
let  mockLog = null;
let mockError = null;
describe('Events Test', () => {
    beforeEach(() => {
        mockLog = sinon.stub(console, 'log');
        mockLog.returns(true);
        mockError = sinon.stub(console, 'error');
        mockError.returns(true);
    });

    afterEach(() => {
        mockLog.restore();
        mockError.restore();
    })

    it('onListen method logs at least one message', () => {
        onListen('1.1.1.1', 3000);
        chai.expect(mockLog.callCount).to.be.eql(1);
    });

    it('onException', () => {

      const error = {
        err:'mock error'
      };
      
      onException(error);
      chai.expect(mockError.firstCall.args[0]).to.be.eql(error);

    });

    it('onServerError', () => {

      onServerError();
      chai.expect(mockLog.firstCall.args[0]).to.be.eq('SERVER ERROR');      

    });

})