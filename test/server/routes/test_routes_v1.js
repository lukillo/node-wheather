const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

describe('Test Route v1', ()=>{

  const mockFn = sinon.stub();
  let mockGet = sinon.stub();

  mockGet.withArgs('/location',mockFn).returns(true);
  mockGet.withArgs('/current/:city?',mockFn).returns(true);
  mockGet.withArgs('/forecast/:city?',mockFn).returns(true);
  
  const mockRouter = ()=>({
    get:mockGet
  });
  
  const router =  proxyquire('../../../src/server/routes/v1.js',{
    'express':{
      Router:mockRouter
    },
    '../controller/location':()=>true,
    '../controller/current':()=>true,
    '../controller/forecast':()=>true
  });

  
  it('GET - /location',  ()=>{
    chai.expect(mockGet.firstCall.args[0]).to.be.equal('/location');
    chai.expect(mockGet.firstCall.args[1]).to.be.a('function');    
  });

  it('GET - /current/:city?',  ()=>{
    chai.expect(mockGet.secondCall.args[0]).to.be.equal('/current/:city?');
    chai.expect(mockGet.secondCall.args[1]).to.be.a('function');    
  });

  it('GET - /forecast/:city?',  ()=>{
    chai.expect(mockGet.thirdCall.args[0]).to.be.equal('/forecast/:city?');
    chai.expect(mockGet.thirdCall.args[1]).to.be.a('function');    
  });

});