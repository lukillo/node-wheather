const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

describe('Test Route Health', ()=>{

  const mockFn = sinon.stub();
  let mockGet = sinon.stub();

  mockGet.withArgs('/',mockFn).returns(true);
  
  const mockRouter = ()=>({
    get:mockGet
  });
  
  const router =  proxyquire('../../../src/server/routes/health.js',{
    'express':{
      Router:mockRouter
    },
    '../controller/health.js':()=>true
  });

  
  it('GET - /',  ()=>{
    chai.expect(mockGet.firstCall.args[0]).to.be.equal('/');
    chai.expect(mockGet.firstCall.args[1]).to.be.a('function');    

  });

});