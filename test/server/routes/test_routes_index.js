const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

const mockHealth = sinon.fake();
const mockV1 = sinon.fake() 
describe('Test Route index', ()=>{
  it('Bind',  ()=>{
    const {
      bind,
      NotFound
    } = proxyquire('../../../src/server/routes/index.js', {
      './health':mockHealth,
      './v1.js': mockV1

    });

    const mockUse =sinon.stub().returns(true);
    const mockGet =sinon.stub().returns(true);
    const app = {
      use: mockUse,
      get: mockGet,
    }
    bind(app);
    chai.expect(mockUse.firstCall.args[0]).to.be.a('function');
    chai.expect(mockUse.secondCall.args).to.be.eql(['/health',  mockHealth]);
    chai.expect(mockUse.thirdCall.args).to.be.eql(['/v1',  mockV1]);
    chai.expect(mockGet.calledOnce).to.be.true;
  })
  it('NotFound',  ()=>{

    const {
      NotFound
    } = require('../../../src/server/routes/index.js');

    let jsonMock = sinon.stub();
    jsonMock.returns(true);

    const req = {};
    const res = {
      status : sinon.fake.returns({
        json: jsonMock
      })
    };

    NotFound(req,res);
    chai.expect(jsonMock.firstCall.args[0]).to.be.eql({ error: 'Route not found' } );

  });

  it('allowCrossDomain',  ()=>{

    const {
      allowCrossDomain
    } = require('../../../src/server/routes/index.js');

    const req = {
      method:'----'
    };

    const res = {
      header: sinon.fake.returns(true)
    };

    let nextStub = sinon.stub();
    nextStub.returns(true);

    allowCrossDomain(req,res,nextStub);
    chai.expect(nextStub.firstCall.args).to.be.eql([]);

  });

});