const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);
const mockLocation ={"status":"success","country":"Argentina","countryCode":"AR","region":"C","regionName":"Buenos Aires F.D.","city":"Buenos Aires","zip":"1871","lat":-34.6021,"lon":-58.3845,"timezone":"America/Argentina/Buenos_Aires","isp":"Telecom Argentina S.A.","org":"Telecom Argentina S.A","as":"AS10481 Telecom Argentina S.A.","query":"190.192.81.14"}
const mockLocationService = sinon.stub().resolves(mockLocation);
const mockIp = sinon.stub();

const location = proxyquire('../../../src/server/controller/location',{
  '../../services/locationService': mockLocationService,
  '../../lib/ip': mockIp
});

describe('Test Location controller', ()=>{
  beforeEach(() => {
    mockIp.resetHistory();
    mockLocationService.resetHistory();
  });

  it('location - Without ip should throw', async ()=>{

    const res = {
    }; 
    const next = sinon.stub();

    const req = {
    };
    mockIp.withArgs({}).returns(null);
  
    await location(req,res, next);
     
    chai.expect(next.firstCall.args[0].message).to.be.eql('Ip is needed for location');
    
  });

  it('location - all ok', async ()=>{

    let responseJson = sinon.stub();
    let mockResponse = {
      json:responseJson
    };

    let responseStatus = sinon.stub();
    responseStatus.withArgs(200).returns(mockResponse);

    const res = {
      status:responseStatus
    };  
    const next = sinon.stub();

    const req = {
      params:{
        clientId:1111
      }
    };
    mockIp.withArgs(req).returns('192.168.0');
  
    await location(req,res, next);
     
    chai.expect(mockIp.calledOnce).to.be.true;
    chai.expect(mockLocationService.calledOnce).to.be.true;
    chai.expect(responseStatus.firstCall.args[0]).to.be.eql(200);
    chai.expect(responseJson.firstCall.args[0]).to.be.eql(mockLocation);
    
  });

});