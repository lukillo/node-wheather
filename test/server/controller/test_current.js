const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);
const mockIp = sinon.stub();
mockIp.withArgs({
  params:{
  }
}).returns('192.168.0.1');
mockIp.withArgs({}).throws({message:'ERROR'});
const mockResponseObj = {
  "cod": "200",
  "message": 0,
  "cnt": 5,
  "list": [
      {
          "dt": 1594414800,
          "main": {
              "temp": 13.17,
              "feels_like": 10.01,
              "temp_min": 13.17,
              "temp_max": 13.3,
              "pressure": 1010,
              "sea_level": 1010,
              "grnd_level": 1006,
              "humidity": 69,
              "temp_kf": -0.13
          },
          "weather": [
              {
                  "id": 804,
                  "main": "Clouds",
                  "description": "overcast clouds",
                  "icon": "04n"
              }
          ],
          "clouds": {
              "all": 100
          },
          "wind": {
              "speed": 3.72,
              "deg": 240
          },
          "sys": {
              "pod": "n"
          },
          "dt_txt": "2020-07-10 21:00:00"
      },
  ],
  "city": {
      "id": 3435910,
      "name": "Buenos Aires",
      "coord": {
          "lat": -34.6132,
          "lon": -58.3772
      },
      "country": "AR",
      "population": 1000000,
      "timezone": -10800,
      "sunrise": 1594378769,
      "sunset": 1594414678
  }
}
const mockGetWheaterByCity = sinon.stub();
mockGetWheaterByCity.withArgs('Buenos%20Aires').resolves(mockResponseObj)
const mockGetWheaterByIp = sinon.stub().withArgs('192.168.0.1').resolves(mockResponseObj);
const mockCurrentService = {
  getWheaterByCity: mockGetWheaterByCity,
  getWheaterByIp: mockGetWheaterByIp
};

const current = proxyquire('../../../src/server/controller/current.js', {
  '../../lib/ip': mockIp,
  '../../services/currentService': mockCurrentService
});

describe('Test Current controller', ()=>{

  it('current - Call ok with city name', async ()=>{

    let responseJson = sinon.stub();
    let mockResponse = {
      json:responseJson
    };

    let responseStatus = sinon.stub();
    responseStatus.withArgs(200).returns(mockResponse);

    const res = {
      status:responseStatus
    };  
    const next = sinon.stub();

    const req = {
      params:{
        city:'Buenos%20Aires',
      }
    };

    await current(req,res, next);
    chai.expect(res.status.firstCall.args[0]).to.be.equal(200);
    chai.expect(mockResponse.json.firstCall.args[0]).to.be.eql(mockResponseObj);
    
  });

  it('current - Call ok without city name', async ()=>{

    let responseJson = sinon.stub();
    let mockResponse = {
      json:responseJson
    };

    let responseStatus = sinon.stub();
    responseStatus.withArgs(200).returns(mockResponse);

    const res = {
      status:responseStatus
    };  
    const next = sinon.stub();

    const req = {
      params:{
      }
    };

    await current(req,res, next);
    chai.expect(res.status.firstCall.args[0]).to.be.equal(200);
    chai.expect(mockResponse.json.firstCall.args[0]).to.be.eql(mockResponseObj);
    
  });

  it('current - next Function called', async ()=>{

    let responseJson = sinon.stub();
    let mockResponse = {
      json:responseJson
    };

    let responseStatus = sinon.stub();
    responseStatus.withArgs(200).returns(mockResponse);

    const res = {
      status:responseStatus
    };  
    const next = sinon.stub();

    const req = {};

    await current(req,res, next);
    chai.expect(next.calledOnce).to.be.true;
    chai.expect(next.firstCall.args[0].message).to.be.eql('ERROR');
    
  });

});