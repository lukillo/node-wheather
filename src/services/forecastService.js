
const getLocation = require('./locationService');
const {fetchForecast} = require('../accessors/forecast');

const getCityNameByIp = async (ip)=>{
    const {city} = await getLocation(ip);
    return encodeURIComponent(city);
}

const getForecastByIp = async(ip) => {
    const cityName = await getCityNameByIp(ip);
    return getForecastByCity(cityName);
}

const getForecastByCity = (cityName)=> fetchForecast(cityName);



module.exports = {
    getForecastByCity,
    getForecastByIp
}