
const getLocation = require('./locationService');
const {fetchWheather} = require('../accessors/wheather');

const getCityNameByIp = async (ip)=>{
    const {city} = await getLocation(ip);
    return encodeURIComponent(city);
}

const getWheaterByIp = async(ip) => {
    const cityName = await getCityNameByIp(ip);
    return getWheaterByCity(cityName);
}

const getWheaterByCity = (cityName)=> fetchWheather(cityName);

module.exports = {
    getWheaterByCity, 
    getWheaterByIp
}