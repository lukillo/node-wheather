const {
    fetchLocation
} = require('../accessors/location');

const getLocation = async (ip)=> await fetchLocation(ip);

module.exports = getLocation;