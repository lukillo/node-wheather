const express = require('express');
const router = express.Router();

//Require health controller.
const health = require('../controller/health.js');

router.get('/', health);

module.exports = router;