const health = require('./health');
const v1 = require('./v1.js');


//404 not found middleware
const NotFound = (req, res) => res.status(404).json({error: "Route not found"});

//Add CORS to middleware.
const allowCrossDomain = (req, res, next) =>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    next();
}

//Bind path with route.
const bind = app => {
    app.use(allowCrossDomain);
    app.use('/health', health);
    app.use('/v1', v1)
    app.get('*', NotFound);
}

module.exports = {
    bind,
    allowCrossDomain,
    NotFound
}