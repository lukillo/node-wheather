const express = require('express');
const router  = express.Router();

//Require controllers.
const location = require('../controller/location');
const current = require('../controller/current');
const forecast = require('../controller/forecast');

router.get('/location', location);
router.get('/current/:city?', current);
router.get('/forecast/:city?', forecast);
module.exports = router;