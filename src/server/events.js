//On server internal error
const onServerError = () => {
    console.log('SERVER ERROR');
}

//On server start
const onListen = (port) => {
    console.log(`Node - wheater Runing in port ${port}`);
}

const onException = err => {
    console.error(err);
}

module.exports = {
    onListen,
    onException,
    onServerError
}