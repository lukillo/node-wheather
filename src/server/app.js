const http = require('http');
const express = require('express');
const routes = require('./routes/index.js');
const bodyParser = require('body-parser');
const config = require('config');

//Define events and routes.
const events = require('./events.js');

const port = config.get('port');

//Start Express-js
const app = express();
const server = http.createServer(app);

app.use(bodyParser.json());
routes.bind(app);

//Srtart listen mode.
app.listen(port, ()=> events.onListen(port));

//Define server special event to hanlde situations.
server.on('error', events.onServerError);
process.on('unhandledRejection', (err)=>events.onException(err));
process.on('uncaughtException', (err)=>events.onException(err));
