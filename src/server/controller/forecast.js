const getIp = require('../../lib/ip');
const {getForecastByCity, getForecastByIp} = require('../../services/forecastService');

/**
 * @api {get} /v1/forecast/:city?
 * @apiGroup Forecast
 * @apiVersion 1.0.0
 * @apiSuccess {String} city optional
 * 
 * @apiSuccessExample {json} res
 * 
 *  {"cod":"200","message":0,"cnt":5,"list":[{"dt":1594501200,"main":{"temp":13.37,"feels_like":8.02,"temp_min":12.01,"temp_max":13.37,"pressure":1012,"sea_level":1011,"grnd_level":1007,"humidity":34,"temp_kf":1.36},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}],"clouds":{"all":28},"wind":{"speed":4.38,"deg":330},"sys":{"pod":"n"},"dt_txt":"2020-07-11 21:00:00"},{"dt":1594512000,"main":{"temp":11.87,"feels_like":7.53,"temp_min":11.1,"temp_max":11.87,"pressure":1013,"sea_level":1013,"grnd_level":1008,"humidity":42,"temp_kf":0.77},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":62},"wind":{"speed":3.24,"deg":301},"sys":{"pod":"n"},"dt_txt":"2020-07-12 00:00:00"},{"dt":1594522800,"main":{"temp":10.79,"feels_like":6.73,"temp_min":10.52,"temp_max":10.79,"pressure":1013,"sea_level":1013,"grnd_level":1008,"humidity":46,"temp_kf":0.27},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":75},"wind":{"speed":2.89,"deg":277},"sys":{"pod":"n"},"dt_txt":"2020-07-12 03:00:00"},{"dt":1594533600,"main":{"temp":10.35,"feels_like":6.2,"temp_min":10.32,"temp_max":10.35,"pressure":1013,"sea_level":1013,"grnd_level":1008,"humidity":46,"temp_kf":0.03},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":89},"wind":{"speed":2.94,"deg":286},"sys":{"pod":"n"},"dt_txt":"2020-07-12 06:00:00"},{"dt":1594544400,"main":{"temp":10.14,"feels_like":4.62,"temp_min":10.14,"temp_max":10.14,"pressure":1013,"sea_level":1013,"grnd_level":1009,"humidity":47,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":100},"wind":{"speed":4.92,"deg":229},"sys":{"pod":"n"},"dt_txt":"2020-07-12 09:00:00"}],"city":{"id":3435910,"name":"Buenos Aires","coord":{"lat":-34.6132,"lon":-58.3772},"country":"AR","population":1000000,"timezone":-10800,"sunrise":1594465151,"sunset":1594501113}}
 * 
 * 
 */

const forecast = async (req, res, next) => {
  try {
      const city = (req.params && req.params.city)? req.params.city : null;
      let wheather = null;
      if (city) 
        wheather = await getForecastByCity(city);
      else {
          const ip = getIp(req);
          wheather = await getForecastByIp(ip)  
      }

      res.status(200).json(wheather);
  } catch (error) {
      next(error);
  }  
}

module.exports = forecast;