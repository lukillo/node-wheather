
const assert = require('assert');
const getLocation = require('../../services/locationService');
const getIp = require('../../lib/ip');

/**
 * @api {get} /v1/location
 * @apiGroup Location
 * @apiVersion 1.0.0
 * @apiSuccess {Number} status_code
 * 
 * @apiSuccessExample {json} res
 * 
 *  {"status":"success","country":"Argentina","countryCode":"AR","region":"C","regionName":"Buenos Aires F.D.","city":"Buenos Aires","zip":"1871","lat":-34.6021,"lon":-58.3845,"timezone":"America/Argentina/Buenos_Aires","isp":"Telecom Argentina S.A.","org":"Telecom Argentina S.A","as":"AS10481 Telecom Argentina S.A.","query":"190.192.81.14"}
 * 
 * 
 */

const location = async (req, res, next)=> {
    try {       
        const ip = getIp(req);
        assert(ip,'Ip is needed for location');

        const location = await getLocation(ip);

        res.status(200).json(location);
    } catch (error) {
      next(error);
    }
}

module.exports = location;