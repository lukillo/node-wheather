const getIp = require('../../lib/ip');
const {getWheaterByCity, getWheaterByIp} = require('../../services/currentService');

/**
 * @api {get} /v1/current/:city?
 * @apiGroup Current
 * @apiVersion 1.0.0
 * @apiSuccess {String} city optional
 * 
 * @apiSuccessExample {json} res
 * 
 *  {
 *   "coord": {
 *       "lon": -58.38,
 *       "lat": -34.61
 *   },
 *   "weather": [
 *       {
 *           "id": 800,
 *           "main": "Clear",
 *           "description": "clear sky",
 *           "icon": "01d"
 *       }
 *   ],
 *   "base": "stations",
 *   "main": {
 *       "temp": 14.12,
 *       "feels_like": 8.87,
 *       "temp_min": 13.89,
 *       "temp_max": 14.44,
 *       "pressure": 1012,
 *       "humidity": 24
 *   },
 *   "visibility": 10000,
 *   "wind": {
 *       "speed": 3.6,
 *       "deg": 320
 *   },
 *   "clouds": {
 *       "all": 0
 *   },
 *   "dt": 1594491966,
 *   "sys": {
 *       "type": 1,
 *       "id": 8224,
 *       "country": "AR",
 *       "sunrise": 1594465151,
 *       "sunset": 1594501114
 *   },
 *   "timezone": -10800,
 *   "id": 3435910,
 *   "name": "Buenos Aires",
 *   "cod": 200
 * }
 */

const current = async (req, res, next) => {
  try {
      const city = (req.params && req.params.city)? req.params.city : null;
      let wheather = null;
      if (city) 
        wheather = await getWheaterByCity(city);
      else {
          const ip = getIp(req);
          wheather = await getWheaterByIp(ip)  
      }

      res.status(200).json(wheather);
  } catch (error) {
      next(error);
  }  
}

module.exports = current;