
/**
 * @api {get} /health
 * @apiGroup HealthCheck
 * @apiVersion 1.0.0
 * @apiSuccess {Number} status_code
 * 
 * @apiSuccessExample {json} res
 * {
 *  "status_code": 200
 * } 
 * 
 */
const health = (req, res)=> {
    res.status(200).json({'status': 'OK'});
}

module.exports = health;