
const config = require('config');
const createError = require('http-errors');
const fetch  = require('node-fetch');
const SUCCESS_CODES = [200];

const check = (res)=>{

    if (SUCCESS_CODES.includes(res.status))
      return res.json();
  
    throw createError.BadGateway();
  
}

const fetchForecast = async (cityName) => {
    
    const url = `${config.get('wheater')}/forecast?q=${cityName}&units=metric&cnt=5&appid=${config.get('apiKey')}`;

    return check( await fetch(url, { method: 'GET'}));
}

module.exports = {
    fetchForecast
}