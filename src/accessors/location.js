
const config = require('config');
const createError = require('http-errors');
const fetch  = require('node-fetch');
const SUCCESS_CODES = [200];

const check = (res)=>{

    if (SUCCESS_CODES.includes(res.status))
      return res.json();
  
    throw createError.BadGateway();
  
}

const fetchLocation = async (ip) => {
    //Get url from config.
    const url = `${config.get('ipApi')}/${ip}`;

    return check( await fetch(url, { method: 'GET'}));
}


module.exports = {
    fetchLocation
}