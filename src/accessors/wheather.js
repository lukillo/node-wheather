
const config = require('config');
const createError = require('http-errors');
const fetch  = require('node-fetch');
const SUCCESS_CODES = [200];

const check = (res)=>{

    if (SUCCESS_CODES.includes(res.status))
      return res.json();
  
    throw createError.BadGateway();
  
}

const fetchWheather = async (cityName) => {
    
    const url = `${config.get('wheater')}/weather?q=${cityName}&units=metric&appid=${config.get('apiKey')}`;

    return check( await fetch(url, { method: 'GET'}));
}

module.exports = {
    fetchWheather
}