define({ "api": [
  {
    "type": "get",
    "url": "/v1/current/:city?",
    "title": "",
    "group": "Current",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>optional</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "res",
          "content": "\n {\n  \"coord\": {\n      \"lon\": -58.38,\n      \"lat\": -34.61\n  },\n  \"weather\": [\n      {\n          \"id\": 800,\n          \"main\": \"Clear\",\n          \"description\": \"clear sky\",\n          \"icon\": \"01d\"\n      }\n  ],\n  \"base\": \"stations\",\n  \"main\": {\n      \"temp\": 14.12,\n      \"feels_like\": 8.87,\n      \"temp_min\": 13.89,\n      \"temp_max\": 14.44,\n      \"pressure\": 1012,\n      \"humidity\": 24\n  },\n  \"visibility\": 10000,\n  \"wind\": {\n      \"speed\": 3.6,\n      \"deg\": 320\n  },\n  \"clouds\": {\n      \"all\": 0\n  },\n  \"dt\": 1594491966,\n  \"sys\": {\n      \"type\": 1,\n      \"id\": 8224,\n      \"country\": \"AR\",\n      \"sunrise\": 1594465151,\n      \"sunset\": 1594501114\n  },\n  \"timezone\": -10800,\n  \"id\": 3435910,\n  \"name\": \"Buenos Aires\",\n  \"cod\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/server/controller/current.js",
    "groupTitle": "Current",
    "name": "GetV1CurrentCity"
  },
  {
    "type": "get",
    "url": "/v1/forecast/:city?",
    "title": "",
    "group": "Forecast",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>optional</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "res",
          "content": "\n{\"cod\":\"200\",\"message\":0,\"cnt\":5,\"list\":[{\"dt\":1594501200,\"main\":{\"temp\":13.37,\"feels_like\":8.02,\"temp_min\":12.01,\"temp_max\":13.37,\"pressure\":1012,\"sea_level\":1011,\"grnd_level\":1007,\"humidity\":34,\"temp_kf\":1.36},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"clouds\":{\"all\":28},\"wind\":{\"speed\":4.38,\"deg\":330},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2020-07-11 21:00:00\"},{\"dt\":1594512000,\"main\":{\"temp\":11.87,\"feels_like\":7.53,\"temp_min\":11.1,\"temp_max\":11.87,\"pressure\":1013,\"sea_level\":1013,\"grnd_level\":1008,\"humidity\":42,\"temp_kf\":0.77},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":62},\"wind\":{\"speed\":3.24,\"deg\":301},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2020-07-12 00:00:00\"},{\"dt\":1594522800,\"main\":{\"temp\":10.79,\"feels_like\":6.73,\"temp_min\":10.52,\"temp_max\":10.79,\"pressure\":1013,\"sea_level\":1013,\"grnd_level\":1008,\"humidity\":46,\"temp_kf\":0.27},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":75},\"wind\":{\"speed\":2.89,\"deg\":277},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2020-07-12 03:00:00\"},{\"dt\":1594533600,\"main\":{\"temp\":10.35,\"feels_like\":6.2,\"temp_min\":10.32,\"temp_max\":10.35,\"pressure\":1013,\"sea_level\":1013,\"grnd_level\":1008,\"humidity\":46,\"temp_kf\":0.03},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":89},\"wind\":{\"speed\":2.94,\"deg\":286},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2020-07-12 06:00:00\"},{\"dt\":1594544400,\"main\":{\"temp\":10.14,\"feels_like\":4.62,\"temp_min\":10.14,\"temp_max\":10.14,\"pressure\":1013,\"sea_level\":1013,\"grnd_level\":1009,\"humidity\":47,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":4.92,\"deg\":229},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2020-07-12 09:00:00\"}],\"city\":{\"id\":3435910,\"name\":\"Buenos Aires\",\"coord\":{\"lat\":-34.6132,\"lon\":-58.3772},\"country\":\"AR\",\"population\":1000000,\"timezone\":-10800,\"sunrise\":1594465151,\"sunset\":1594501113}}",
          "type": "json"
        }
      ]
    },
    "filename": "src/server/controller/forecast.js",
    "groupTitle": "Forecast",
    "name": "GetV1ForecastCity"
  },
  {
    "type": "get",
    "url": "/health",
    "title": "",
    "group": "HealthCheck",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status_code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "res",
          "content": "{\n \"status_code\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/server/controller/health.js",
    "groupTitle": "HealthCheck",
    "name": "GetHealth"
  },
  {
    "type": "get",
    "url": "/v1/location",
    "title": "",
    "group": "Location",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status_code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "res",
          "content": "\n{\"status\":\"success\",\"country\":\"Argentina\",\"countryCode\":\"AR\",\"region\":\"C\",\"regionName\":\"Buenos Aires F.D.\",\"city\":\"Buenos Aires\",\"zip\":\"1871\",\"lat\":-34.6021,\"lon\":-58.3845,\"timezone\":\"America/Argentina/Buenos_Aires\",\"isp\":\"Telecom Argentina S.A.\",\"org\":\"Telecom Argentina S.A\",\"as\":\"AS10481 Telecom Argentina S.A.\",\"query\":\"190.192.81.14\"}",
          "type": "json"
        }
      ]
    },
    "filename": "src/server/controller/location.js",
    "groupTitle": "Location",
    "name": "GetV1Location"
  }
] });
