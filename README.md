# node-wheater

### **Comandos**:
- Correr test:
```console
npm run test
```
- Covertura de codigo:
```console
 npm run coverage
---------------------|---------|----------|---------|---------|-------------------
File                 | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s 
---------------------|---------|----------|---------|---------|-------------------
All files            |     100 |      100 |     100 |     100 |                   
 accessors           |     100 |      100 |     100 |     100 |                   
  forecast.js        |     100 |      100 |     100 |     100 |                   
  location.js        |     100 |      100 |     100 |     100 |                   
  wheather.js        |     100 |      100 |     100 |     100 |                   
 lib                 |     100 |      100 |     100 |     100 |                   
  ip.js              |     100 |      100 |     100 |     100 |                   
 server              |     100 |      100 |     100 |     100 |                   
  events.js          |     100 |      100 |     100 |     100 |                   
 server/controller   |     100 |      100 |     100 |     100 |                   
  current.js         |     100 |      100 |     100 |     100 |                   
  forecast.js        |     100 |      100 |     100 |     100 |                   
  health.js          |     100 |      100 |     100 |     100 |                   
  location.js        |     100 |      100 |     100 |     100 |                   
 server/routes       |     100 |      100 |     100 |     100 |                   
  health.js          |     100 |      100 |     100 |     100 |                   
  index.js           |     100 |      100 |     100 |     100 |                   
  v1.js              |     100 |      100 |     100 |     100 |                   
 services            |     100 |      100 |     100 |     100 |                   
  currentService.js  |     100 |      100 |     100 |     100 |                   
  forecastService.js |     100 |      100 |     100 |     100 |                   
  locationService.js |     100 |      100 |     100 |     100 |                   
---------------------|---------|----------|---------|---------|-------------------
```
- Levantar aplicacion:
```console
 npm run serve
```
- Generar imagen de docker:
```console
 docker build -t node-wheather .
```
- Correr imagen de docker:
```console
  docker run -p 3000:3000 -d node-wheather
```
### **Documentación apis**:
En /doc/index.html se encuentra toda la documentacion sobre como llamar a las apis y las respuestas que pueden generar.
